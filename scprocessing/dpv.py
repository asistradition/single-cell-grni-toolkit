import scanpy as _sc
import numpy as _np
import scipy as _sp
from anndata import AnnData as _AnnData
from scipy.sparse import issparse as _issparse
import scipy.sparse as _scs
import scipy.sparse.csr as _csr
from scvelo.preprocessing.neighbors import select_connectivities as _select_connectivities, select_distances as _select_distances
import sklearn.linear_model as _linear_models
import warnings
from sklearn.metrics import mean_squared_error as _mse, r2_score as _r2


def extract_trajectory_cells(adata, trajectory, min_cells=0, min_genes=0, celltypes='celltype'):
    """
    Extract only cells related to a specific trajectory specified on the adata.uns['trajectory']

    :param adata: an AnnData object
    :param trajectory: name of trajectory
    :param min_cells: filter genes based on how many cells they are expressed in after cells in trajectory is selected.
    """

    if isinstance(trajectory, int):
        trajectory = list(adata.uns['trajectory'].keys())[trajectory]

    trjac_cells = adata.obs[celltypes].isin(adata.uns['trajectory'][trajectory]).values
    trdata = adata[trjac_cells, :].copy()

    if min_cells != 0 or min_cells is not None:
        _sc.pp.filter_genes(trdata, min_cells=min_cells)

    if min_genes != 0 or min_genes is not None:
        _sc.pp.filter_cells(trdata, min_genes=10)

    __ = trdata.obs['dpt_pseudotime'].argsort().values
    tmp = _np.zeros(__.shape)
    for i, j in enumerate(__):
        tmp[j] = i

    trdata.obs['dpt_order_indices'] = tmp.astype(int)

    return trdata


def get_neighbors(adata, i, get_distances=False):
    """Get neighbor indices for cell i.

    :param adata: AnnData object
    :param i: cell of interest
    :param get_distances: also get the raw distances to neighbors. Not implemented yet.
    :returns: neighbor indices
    :rtype: numpy array

    """
    # neighbors = sc.Neighbors(adata)

    if isinstance(adata, _sc.neighbors.Neighbors):
        neighbor_indices = adata.distances[i, :]
    else:
        neighbor_indices = adata.uns['neighbors']['distances'][i, :]

    neighbor_indices = neighbor_indices.sum(0).nonzero()
    # neighbor_distances = adata.uns['neighbors']['distances'][neighbor_indices[0], neighbor_indices[1]]
    neighbor_indices = _np.unique(_np.append(i, neighbor_indices[1]))

    if get_distances:
        raise NotImplementedError("Can't get distances currently")
    else:
        return neighbor_indices


def extract_neighbor_data(data, neighbor_indices, use_var=None, use_layer='X'):

    adata = data.raw if (use_layer == 'raw') else data

    use_variable = False
    if use_var is not None:
        use_variable = True
        variables = [i for i in use_var if i in adata.var_names]

    adata_comp = adata[:, variables] if use_variable else adata

    if use_layer == 'X' or use_layer == 'raw':
        X = adata_comp[neighbor_indices, :].X.copy()
    else:
        X = adata_comp[neighbor_indices, :].layers[use_layer].copy()

    return X


def get_relative_dpt(time_points, center_index=0):

    shifted_time_points = time_points - time_points[center_index]

    return shifted_time_points


def get_relative_distances(order, distances, center_index=0):
    """DEPRECATED!

    :param order: 
    :param distances: 
    :param center_index: 
    :returns: 
    :rtype: 

    """

    if _np.abs(center_index) > len(order):
        raise ValueError(f"center_index={center_index} must be lower than number of neighbors = {len(order)}.")

    distances[order < order[center_index]] = -distances[order < order[center_index]]

    return distances


def get_neighbor_distances(neighbors, neighbor_indices, celli, n_neighbors=None, max_c=True, scale_max=True, reversed=False):

    if n_neighbors is None:
        n_neighbors = neighbors.n_neighbors

    t0 = neighbors.distances_dpt[neighbors.iroot]

    t1 = neighbors.distances_dpt[celli]

    if scale_max:
        t0 = t0 / t0.max()
        t1 = t1 / t1.max()

    if reversed:
        deltat = t0[celli] - t0
    else:
        deltat = t0 - t0[celli]

    t1 = t1 * _np.sign(deltat)

    delta_times = t1[neighbor_indices]

    if max_c:
        closest_indices = _np.argsort(_np.abs(delta_times))[:min(n_neighbors, len(neighbor_indices))]
        delta_times = delta_times[closest_indices]
        neighbor_indices = neighbor_indices[closest_indices]

    return (delta_times, neighbor_indices)


def extract_expression_and_dpt(adata, cell, use_genes=None, use_layer='X', relative_time=True, n_neighbors=None, max_c=True, neighbors=None, scale_max=True):
    """Extracts expression values and diffusion pseudo time for neighbors of a specific cell.

    :param adata: AnnData object
    :param cell: what cell should be the center
    :param use_genes: use only the genes in this list
    :param use_raw: use raw expression
    :param relative_time: calculate relative time to cell.
    :param n_neighbors: try to extract this many neighbors
    :param max_c: force max n_neighbors using only the closest.
    :returns: Y, x
    :rtype: numpy.array

    """

    if isinstance(cell, str):
        celli = _np.where(adata.obs_names == cell)[0][0]
    elif isinstance(cell, int):
        celli = cell
    else:
        raise ValueError(f"cell imput must be int or str. Currently its {type(cell)}")

    if neighbors is None:
        neighbors = _sc.Neighbors(adata)

    neighbor_indices = get_neighbors(neighbors, celli)
    cell_neighors = neighbor_indices.shape[0]

    while neighbor_indices.shape[0] < n_neighbors:
        neighbor_indices = get_neighbors(neighbors, neighbor_indices)

        if neighbor_indices.shape[0] == 1:  # orphan cell.
            break

        if cell_neighors == neighbor_indices.shape[0]:  # No new neighbors could be found
            break

        cell_neighors = neighbor_indices.shape[0]

    x, neighbor_indices = get_neighbor_distances(neighbors, neighbor_indices, celli, n_neighbors=n_neighbors, max_c=max_c, scale_max=scale_max)

    Y = extract_neighbor_data(adata, neighbor_indices, use_var=use_genes, use_layer=use_layer)

    return Y, x


def gene_rate(Y, x, regressf=_linear_models.LinearRegression, impute_velocity=False, center_cell=None, swap_nan=True, calc_error=False, **kwfit):
    """Calculate the rate of change for each gene at each cell given it's neighboring cells.

    :param Y: gene expression, dimension (samples) x (genes)
    :param x: time points, dimension (samples)
    :regressf: function object, sklearn _linear_models.LinearRegression by default. Should accept two parameters (expr, time) as input.
    :inpute_velocity: regress velocity when gene expression for center gene = 0. Bool, default False.
    :center_cell: if supplied will be the index of the time vector where the center cell is assigned, default None. where delta t==0
    :returns: vel, offset
    :rtype: float

    """

    if center_cell is None:
        center_cell = _np.where(x == 0)[0]
        if len(center_cell) > 1:
            center_cell = center_cell[0]  # It turns out some cells have the exact same diffusion time and are neighbors meaning that they produce artificats in the data.

    was_sparse = False
    if _issparse(Y):
        Y = Y.toarray()
        was_sparse = True

    where_not_zero = _np.array((Y[center_cell, :] != 0).astype(int)).squeeze()

    reg_fit = regressf(**kwfit)

    x = x.reshape(-1, 1)
    reg_fit = reg_fit.fit(x, Y)

    if hasattr(reg_fit, 'estimator_'):
        reg_fit = reg_fit.estimator_

    offset = reg_fit.intercept_
    vel = reg_fit.coef_.reshape(-1, )

    if calc_error:

        predicted = reg_fit.predict()

        se = _np.square(Y - predicted).sum(0).reshape(-1, ) / x.shape[0]
        me = _np.square(Y - Y.mean(0)).sum(0).reshape(-1, ) / x.shape[0]

    if not impute_velocity:
        offset = offset * where_not_zero
        vel = vel * where_not_zero
        if calc_error:
            se = se * where_not_zero
            me = me * where_not_zero

    if swap_nan:
        pass
        # r2[_np.isnan(r2)] = 0
        # r2adj[_np.isnan(r2adj)] = 0
        # F[_np.isnan(F)] = 0
        # F_p[_np.isnan(F_p)] = 1

    if was_sparse or impute_velocity:
        if calc_error:
            return _scs.csr.csr_matrix(vel), _scs.csr.csr_matrix(offset), _scs.csr.csr_matrix(se), _scs.csr.csr_matrix(me)
        else:
            return _scs.csr.csr_matrix(vel), _scs.csr.csr_matrix(offset)
    else:
        if calc_error:
            return vel, offset, se, me
        else:
            return vel, offset


def pseudo_velocity(data,
                    n_neighbors=None,
                    use_layer='X',
                    add_layer='velocity',
                    offset_layer='offset',
                    regressf=_linear_models.LinearRegression,
                    impute_velocity=False,
                    max_c=True, copy=False,
                    verbose=False,
                    **kwfit):
    """Calculate regression coefficients on local neighborhood cells along diffusion pseudo time trajectory.

    :param data: AnnData object.
    :param use_raw: if raw expression values should be used.
    :param n_neighbors: number of closesst neighbors to use. If this is larger than the used number of neighbors this will traverse the network neighborhood tree until these are found.
    :param regressf: what regression function should be used to infer gradient in neighborhood.
    :param impute_velocity: if velocity should be calculated for genes with expression 0.
    :param max_c: cap closest neighbors as n_neighbors limit and not use all traversed neighbors.
    :param copy: to copy to new AnnData object.
    :returns: None if copy=False else AnnData.
    :rtype: None or AnnData.

    """

    adata = data.copy() if copy else data

    neighbors = _sc.Neighbors(adata)

    if n_neighbors is None:
        n_neighbors = neighbors.n_neighbors
        n_neighbors = adata.uns['neighbors']['params']['n_neighbors']

    velobs = {}
    velobs['cell'] = []
    velobs[add_layer] = []
    velobs[offset_layer] = []
    for i, cell in enumerate(adata.obs_names):
        if verbose:
            print(f'Working on cell {cell} = {i}/{adata.obs_names.shape[0]} = {100*i/(adata.obs_names.shape[0]-1):.3f}%', end='\r')

        Y, x = extract_expression_and_dpt(adata, cell, use_layer=use_layer, n_neighbors=n_neighbors, max_c=max_c)

        cellvals = gene_rate(Y, x, regressf=regressf, impute_velocity=impute_velocity, **kwfit)

        velobs['cell'].append(cell)
        velobs[add_layer].append(cellvals[0])
        velobs[offset_layer].append(cellvals[1])

    for k, v in velobs.items():
        if k != 'cell':
            if _issparse(velobs[k][0]):
                adata.layers[k] = _scs.vstack(v, format='csr')
            else:
                adata.layers[k] = _np.array(v)

    return adata if copy else None


def trajectory_pv(data, trajectories, min_cells=10, use_layer='X', **pvargs):

    adata = data.copy()
    rows = adata.obs_names
    cols = adata.var_names

    if use_layer == 'X':
        is_sparse = _issparse(adata.X)
    elif use_layer == 'raw':
        is_sparse = _issparse(adata.raw.X)
    else:
        is_sparse = _issparse(adata.layers[use_layer])

    if is_sparse:
        velocity = _scs.csr.csr_matrix((rows.shape[0], cols.shape[0]), dtype='float64')
        offset = _scs.csr.csr_matrix((rows.shape[0], cols.shape[0]), dtype='float64')
    else:
        velocity = _np.zeros((rows.shape[0], cols.shape[0]), dtype='float64')
        offset = _np.zeros((rows.shape[0], cols.shape[0]), dtype='float64')

    cell_members = _np.zeros(adata.obs_names.shape)
    for trajectory in trajectories:

        trdata = extract_trajectory_cells(adata, trajectory, min_cells=min_cells)
        pseudo_velocity(trdata, use_layer=use_layer, **pvargs)

        origrows = _np.array([i for i, r in enumerate(rows) if r in trdata.obs_names])
        origcols = _np.array([i for i, c in enumerate(cols) if c in trdata.var_names])

        cell_members[origrows] = cell_members[origrows] + 1

        velocity = add_sub_matrices(velocity, trdata.layers['velocity'], origrows, origcols)
        offset = add_sub_matrices(offset, trdata.layers['offset'], origrows, origcols)

    adata.layers['velocity'] = (velocity.T / cell_members).T
    adata.layers['offset'] = (offset.T / cell_members).T

    return adata


def add_sub_matrices(background, data, original_rows, original_cols):

    data_sparse = False
    if _issparse(data):
        nzrows, nzcols = data.nonzero()
        nzdata = data.data
        data_sparse = True
    else:
        nzrows, nzcols = _np.nonzero(data)
        nzdata = data

    if _issparse(background):
        # nzrows, nzcols = data.nonzero()
        # nzdata = data.data

        nzrows_or = original_rows[nzrows]
        nzcols_or = original_cols[nzcols]

        if data_sparse:
            background[nzrows_or, nzcols_or] = background[nzrows_or, nzcols_or] + nzdata
        else:
            background[nzrows_or, nzcols_or] = background[nzrows_or, nzcols_or] + nzdata[nzrows, nzcols]

    else:
        # nzrows, nzcols = _np.nonzero(data)
        nzrows_or = original_rows[nzrows]
        nzcols_or = original_cols[nzcols]

        if data_sparse:
            background[nzrows_or, nzcols_or] = background[nzrows_or, nzcols_or] + nzdata
        else:
            background[nzrows_or, nzcols_or] = background[nzrows_or, nzcols_or] + nzdata[nzrows, nzcols]

    return background


def local_mean_in_trajectory(data: _AnnData, trajectories, use_layer='X', min_cells=10, add_layer='Ms'):

    adata = data.copy()

    rows = adata.obs_names
    cols = adata.var_names

    if use_layer == 'X':
        is_sparse = _issparse(adata.X)
    elif use_layer == 'raw':
        is_sparse = _issparse(adata.raw.X)
    else:
        is_sparse = _issparse(adata.layers[use_layer])

    if is_sparse:
        mean_knn = _scs.csr.csr_matrix((rows.shape[0], cols.shape[0]), dtype='float64')
    else:
        mean_knn = _np.zeros((rows.shape[0], cols.shape[0]), dtype='float64')

    cell_members = _np.zeros(adata.obs_names.shape)
    for trajectory in trajectories:

        trdata = extract_trajectory_cells(adata, trajectory, min_cells=min_cells)
        local_smoothing(trdata, use_layer=use_layer, add_layer=add_layer, copy=False)

        origrows = _np.array([i for i, r in enumerate(rows) if r in trdata.obs_names])
        origcols = _np.array([i for i, c in enumerate(cols) if c in trdata.var_names])
        cell_members[origrows] = cell_members[origrows] + 1

        mean_knn = add_sub_matrices(mean_knn, trdata.layers[add_layer], origrows, origcols)

    adata.layers[add_layer] = (mean_knn.T / cell_members).T

    return adata


def apply_smoothing(X, connectivities, keep0=False, type='mean'):

    if type == 'mean':
        if (connectivities.nnz / _np.prod(connectivities.nnz)) < 0.4:  # try to keep sparsity in data
            if keep0:
                Xtmp = _np.abs(X).astype(bool).astype(float).copy()
                X_out = _scs.csr.csr_matrix.dot(connectivities, _scs.csr.csr_matrix(X)).astype(_np.float32).copy()
                X_out = _np.multiply(X_out, Xtmp)
            else:
                X_out = _scs.csr.csr_matrix.dot(connectivities, _scs.csr.csr_matrix(X)).astype(_np.float32).copy()
        else:
            if keep0:
                Xtmp = _np.abs(X).astype(bool).astype(float).copy()
                X_out = _scs.csr.csr_matrix.dot(connectivities, _scs.csr.csr_matrix(X)).astype(_np.float32).A.copy()
                X_out = _np.multiply(X_out, Xtmp)
            else:
                X_out = _scs.csr.csr_matrix.dot(connectivities, _scs.csr.csr_matrix(X)).astype(_np.float32).A.copy()

    elif type == 'median':
        raise NotImplementedError

    return X_out


def local_smoothing(data, use_layer='X', add_layer='Ms', n_neighbors=None, mode='connectivities', recurse_neighbors=None, gathering_steps=1, copy=False, weighted=False, thresholding=True, keep0=False, is_large=False, decay=1, set_diag=0, only_tune=False, symmetrize=False):

    adata = data.copy() if copy else data
    if use_layer == 'X':
        X = adata.X
    elif use_layer == 'raw':
        X = adata.raw.X
    else:
        X = adata.layers[use_layer]

    if n_neighbors is None:
        if 'neighbors' in adata.uns_keys():
            n_neighbors = adata.uns['neighbors']['params']['n_neighbors']
        else:
            warnings.warn("neighborhood not computed, is now run with default parameters", UserWarning)
            _sc.pp.neighbors(adata)
            n_neighbors = adata.uns['neighbors']['params']['n_neighbors']

    connectivities = get_connectivities(adata, mode=mode, n_neighbors=n_neighbors, recurse_neighbors=recurse_neighbors, weighted=weighted, decay=decay, set_diag=set_diag, symmetrize=symmetrize)

    if _issparse(connectivities):
        connectivities = connectivities ** gathering_steps
    else:
        connectivities = _np.linalg.matrix_power(connectivities, gathering_steps)

    if set_diag is not None:
        connectivities.setdiag(set_diag)

    optimal = 0  # default value.

    if thresholding:
        if isinstance(thresholding, (_np.ndarray, list)):
            pred_err = tune_smoothing(X, connectivities, keep0=keep0, set_diag=set_diag)
            if only_tune:
                return pred_err

            optimal = _np.where(pred_err == _np.min(pred_err))[0][0]

            connectivities = threshold_knn(connectivities, thresholding[optimal])
        else:
            connectivities = threshold_knn(connectivities, thresholding)

    X_out = apply_smoothing(X, connectivities, keep0=keep0)

    adata.layers[add_layer] = X_out

    pred_r2 = _r2(X.A if _issparse(X) else X, X_out.A if _issparse(X_out) else X_out)
    pred_err = _mse(X.A if _issparse(X) else X, X_out.A if _issparse(X_out) else X_out)

    adata.uns['smoothing'] = {}
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"] = {}
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["mode"] = mode
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["gathering_steps"] = gathering_steps
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["decay"] = decay
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["n_neighbors"] = n_neighbors
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["weighted"] = weighted
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["thresholding"] = thresholding
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["keep0"] = keep0
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["mse"] = pred_err
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["r2"] = pred_r2
    adata.uns['smoothing'][f"{use_layer} to {add_layer}"]["optimal"] = optimal

    return adata if copy else None


def tune_smoothing(X, connectivities, thresholdings, keep0=False, set_diag=0):

    # X_masked, X_target = n2s.noise2self_Xsplit(X, mean=0.3)
    if set_diag is not None:
        connectivities.setdiag(set_diag)

    cs = connectivities.sum(1)
    cs[cs == 0] = 1
    connectivities = _csr.csr_matrix(connectivities.multiply(1. / cs))

    prediction_error = []
    for th in thresholdings:
        conns = threshold_knn(connectivities.copy(), th)
        X_out = apply_smoothing(X, conns, keep0=keep0)
        # pred_err = _mse(X.toarray(), X_out)
        pred_err = _mse(X.A if _issparse(X) else X, X_out.A if _issparse(X_out) else X_out)
        # X_out = apply_smoothing(X_masked, conns, keep0=keep0)
        # pred_err = _mse(X_target.A if _issparse(X_target) else X_target, X_out.A if _issparse(X_out) else X_out)
        # pred_err = _mse(X_target.toarray(), X_out)
        prediction_error.append(pred_err)

    return prediction_error


def threshold_knn(knng, thresholding):

    if isinstance(thresholding, bool):  # set to scale of contribution depending on number of total nodes
        threshold = 1 / knng.shape[0]
        knngth = knng.copy()
        knngth.data[knngth.data < threshold] = 0
    elif thresholding > 1:  # set to number of maximum neighbors
        thresholding = _np.round(thresholding).astype(int)
        threshold = _sp.array([i.data[_sp.argsort(-i.data)][min(thresholding, i.size - 1)] for i in knng])

        C = []
        for th, r in zip(threshold, knng):
            r.data[r.data <= th] = 0
            C.append(r)

        knngth = _scs.vstack(C).copy()

    else:  # set to minimum contribution
        threshold = 1 / thresholding
        knngth = knng.copy()
        knngth.data[knngth.data < threshold] = 0

    knngth = knngth.multiply(1. / knngth.sum(1))

    knngth = _csr.csr_matrix(knngth)
    knngth.eliminate_zeros()

    return knngth


def get_connectivities(adata: _AnnData, mode='connectivities', n_neighbors=None, recurse_neighbors=False, weighted=False, decay=1, set_diag=0, symmetrize=False):
    C = adata.uns['neighbors'][mode].copy()
    if n_neighbors is not None and n_neighbors < adata.uns['neighbors']['params']['n_neighbors']:
        C = _select_connectivities(C, n_neighbors) if mode == 'connectivities' else _select_distances(C, n_neighbors)

    if weighted:
        if mode == 'distances':
            C.data = _sp.exp(-1 * _sp.power((C / C.data.mean()).data, decay))
            if symmetrize:
                C = C + C.T  # Symmetrize to get rid of complex eigenvalues.

        else:
            C.data = _sp.power(C.data, decay)
            if symmetrize:
                C = C + C.T  # Symmetrize to get rid of complex eigenvalues.

        cs = C.sum(1)
        cs[cs == 0] = 1
        connectivities = C.multiply(1. / cs)

    else:
        connectivities = (C > 0).astype(float)

    if set_diag is not None:
        connectivities.setdiag(set_diag)

    if recurse_neighbors:
        connectivities += connectivities.dot(connectivities * .5)
        connectivities.data = _sp.clip(connectivities.data, 0, 1)

    connectivities = connectivities.multiply(1. / connectivities.sum(1))
    connectivities.eliminate_zeros()

    return connectivities.tocsr().astype(_sp.float32)


def gene_squared_error(args):
    pass
