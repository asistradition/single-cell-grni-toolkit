import numpy as _np
import pandas as _pd
from anndata import AnnData as _AnnData
from scipy.sparse import issparse as _issparse, spmatrix as _spmatrix
from scanpy.preprocessing._simple import pca as _pca
import scanpy.utils as _scutils
import scanpy as _sc
from umap import UMAP as _UMAP
from typing import Optional as _Optional, Union as _Union, Type as _Type
import leidenalg as _leidenalg
from igraph import Graph as _Graph
from leidenalg.VertexPartition import MutableVertexPartition as _MutableVertexPartition
from natsort import natsorted as _natsorted


def cluster_genes_in_trajectory(data: _Union[_AnnData, _np.ndarray, _spmatrix],
                                trajectory=None,
                                pca_comps: _Optional[int] = None,
                                umap_comps=2,
                                copy=False,
                                pca_solver='auto',
                                random_state: _Optional[int] = 0,
                                return_info=False,
                                metric='euclidean',
                                **umap_kwargs):

    data_is_AnnData = isinstance(data, _AnnData)
    if data_is_AnnData:
        adata = data.copy() if copy else data
        X = adata.X.T.copy()

        if trajectory is not None:
            if 'trajectory' not in adata.uns_keys():
                raise ValueError('trajectories needs to be defined in adata.uns["trajectory"]')
            elif trajectory not in adata.uns['trajectory']:
                raise ValueError(f'{trajectory} is not in adata.uns["trajectory"]')
            else:
                trjac_cells = adata.obs['celltype'].isin(adata.uns['trajectory'][trajectory]).values
                X = X[:, trjac_cells]

    else:
        X = data.T.copy()

    ncells = X.shape[1]

    if pca_comps is not None:
        X = _pca(X, n_comps=pca_comps, svd_solver=pca_solver, random_state=random_state, copy=True)

    umap = _UMAP(n_components=umap_comps, random_state=random_state, metric=metric, **umap_kwargs)

    umap.fit(X)

    if data_is_AnnData:
        if trajectory is None:
            trajectory = ""

        adata.varm['X_umap_' + trajectory] = umap.embedding_
        adata.uns['gene_trajectory_' + trajectory] = {}
        adata.uns['gene_trajectory_' + trajectory]['parames'] = {'method': 'umap', 'n_neighbors': umap.n_neighbors, 'n_pcs': pca_comps, 'n_cells': ncells, 'metric': metric}
        adata.uns['gene_trajectory_' + trajectory]['graph'] = umap.graph_

        return adata if copy else None
    else:
        if return_info:
            return umap
        else:
            return umap.embedding_


def leiden_trajectory_cluster(data: _Union[_AnnData, _Graph, _spmatrix],
                              trajectory=None,
                              resolution: float = 1,
                              random_state: int = 0,
                              key_added: str = 'leiden',
                              adjacency: _Optional[_spmatrix] = None,
                              directed: bool = True,
                              use_weights: bool = True,
                              n_iterations: int = -1,
                              partition_type: _Optional[_Type[_MutableVertexPartition]] = None,
                              copy: bool = False,
                              return_info=False,
                              **partition_kwargs):

    data_is_AnnData = isinstance(data, _AnnData)
    if data_is_AnnData:
        adata = data.copy() if copy else data

        if trajectory is not None:
            if 'gene_trajectory_' + trajectory not in adata.uns_keys():
                raise ValueError(f'graph needs to be calculated for trajectory {trajectory} in adata.uns["gene_trajectory_{trajectory}"]')
            else:
                g = adata.uns['gene_trajectory_' + trajectory]['graph']
        else:
            g = adata.uns['gene_trajectory_']['graph']
    else:
        g = data.copy()

    if isinstance(g, _spmatrix):
        g = _scutils.get_igraph_from_adjacency(g, directed=directed)

    if partition_type is None:
        partition_type = _leidenalg.RBConfigurationVertexPartition

    if partition_kwargs is None:
        partition_kwargs = {}

    if use_weights:
        partition_kwargs['weights'] = _np.array(g.es['weight']).astype(_np.float64)
        partition_kwargs['n_iterations'] = n_iterations
        partition_kwargs['seed'] = random_state
    if resolution is not None:
        partition_kwargs['resolution_parameter'] = resolution

    part = _leidenalg.find_partition(g, partition_type, **partition_kwargs)
    groups = _np.array(part.membership)

    if data_is_AnnData:

        if trajectory is None:
            trajectory = ""

        key_added = trajectory + "_" + key_added
        adata.var[key_added] = _pd.Categorical(values=groups.astype('U'), categories=_natsorted(_np.unique(groups).astype('U')))

        adata.uns[key_added] = {}
        adata.uns[key_added]['params'] = dict(
            resolution=resolution,
            random_state=random_state,
            n_iterations=n_iterations)

        return adata if copy else None

    else:
        if return_info:
            return part
        else:
            return groups
