import numpy as _np
import scanpy.logging as _logg
from anndata import AnnData as _AnnData
import scipy.sparse as _scs
from scipy.sparse import issparse as _issparse
from scipy.special import expit as _expit
from sklearn.preprocessing import minmax_scale as _minmax_scale
from scipy.sparse import csr as _csr

_N_ICS = 30


def ica(data, n_comps=None, algorit='parallel', fun='logcosh', fun_args=None, random_state=0, return_info=False, use_highly_variable=None, dtype='float32', copy=False, **kwargs):
    """Independent component analysis
    [https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.FastICA.html]_.

    Computes ICA coordinates and components. Uses the implementation of *scikit-learn* [https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.FastICA.html]_.

    Parameters
    ----------
    data : :class:`~anndata.AnnData`, `np.ndarray`, `sp.sparse`
        The (annotated) data matrix of shape `n_obs` × `n_vars`. Rows correspond
        to cells and columns to genes.
    n_comps : `int`, optional (default: 5)
        Number of principal components to compute.
    algorithm : {'parallel', 'deflation'}
        Apply parallel or deflational algorithm for FastICA.
    whiten : boolean, optional
        If whiten is false, the data is already considered to be
        whitened, and no whitening is performed.
    fun : string or function, optional. Default: 'logcosh'
        The functional form of the G function used in the
        approximation to neg-entropy. Could be either 'logcosh', 'exp',
        or 'cube'.
        You can also provide your own function. It should return a tuple
        containing the value of the function, and of its derivative, in the
        point. Example:
        def my_g(x):
            return x ** 3, 3 * x ** 2
    fun_args : dictionary, optional
        Arguments to send to the functional form.
        If empty and if fun='logcosh', fun_args will take value
        {'alpha' : 1.0}.
    random_state : `int`, optional (default: 0)
        Change to use different intial states for the optimization.
        Warning: ICA algorithm is unstable and may not yield consistent
        result if random_state is not supplied with a constant.
    return_info : `bool`, optional (default: `False`)
        Only relevant when not passing an :class:`~anndata.AnnData`: see
        "Returns".
    use_highly_variable : `bool`, optional (default: `None`)
        Whether to use highly variable genes only, stored in .var['highly_variable'].
    dtype : `str` (default: 'float32')
        Numpy data type string to which to convert the result.
    copy : `bool`, optional (default: `False`)
        If an :class:`~anndata.AnnData` is passed, determines whether a copy
        is returned. Is ignored otherwise.
    **kwargs : will be passed along to sklearn.decomposition.FastICA

    Returns
    -------
    If `data` is array-like and `return_info == False`, only returns `X_ica`,\
    otherwise returns or adds to `adata`:
    X_ica : `.obsm`
         ICA representation of data.
    ICs : `.varm`
         The independent components containing the loadings.
    """

    if n_comps is None:
        n_comps = _N_ICS

    if isinstance(data, _AnnData):
        data_is_AnnData = True
        adata = data.copy() if copy else data
    else:
        data_is_AnnData = False
        adata = _AnnData(data)

    _logg.msg('computing ICA with n_comps =', n_comps, r=True, v=4)

    if adata.n_vars < n_comps:
        n_comps = adata.n_vars - 1
        _logg.msg('reducing number of computed PCs to',
                  n_comps, 'as dim of data is only', adata.n_vars, v=4)

    if use_highly_variable is True and 'highly_variable' not in adata.var.keys():
        raise ValueError('Did not find adata.var[\'highly_variable\']. '
                         'Either your data already only consists of highly-variable genes '
                         'or consider running `pp.filter_genes_dispersion` first.')

    if use_highly_variable is None:
        use_highly_variable = True if 'highly_variable' in adata.var.keys() else False
    adata_comp = adata[:, adata.var['highly_variable']] if use_highly_variable else adata

    from sklearn.decomposition import FastICA as ICA

    if _issparse(adata_comp.X):
        _logg.msg('    ICA requires non sparse data, '
                  '    sparse input is densified and may '
                  '    lead to huge memory consumption', v=4)

    X = adata_comp.X.toarray()  # Copying the whole adata_comp.X here, could cause memory problems
    ica_ = ICA(n_components=n_comps, fun=fun, fun_args=fun_args, random_state=random_state, **kwargs)

    X_ica = ica_.fit_transform(X)

    if X_ica.dtype.descr != _np.dtype(dtype).descr:
        X_ica = X_ica.astype(dtype)

    if data_is_AnnData:
        adata.obsm['X_ica'] = X_ica
        if use_highly_variable:
            adata.varm['ICs'] = _np.zeros(shape=(adata.n_vars, n_comps))
            adata.varm['ICs'][adata.var['highly_variable']] = ica_.components_.T
        else:
            adata.varm['ICs'] = ica_.components_.T

        # Does any additional info of ICA deserve to be stored here?
        # adata.uns['ica'] = {}
        # adata.uns['ica']['variance'] = ica_.explained_variance_
        # adata.uns['ica']['variance_ratio'] = ica_.explained_variance_ratio_
        _logg.msg('    finished', t=True, end=' ', v=4)
        _logg.msg('and added\n'
                  '    \'X_ica\', the PCA coordinates (adata.obs)\n'
                  '    \'PC1\', \'PC2\', ..., the loadings (adata.var)\n'
                  '    \'ica_variance\', the variance / eigenvalues (adata.uns)\n'
                  '    \'ica_variance_ratio\', the variance ratio (adata.uns)', v=4)
        return adata if copy else None
    else:
        if return_info:
            return X_ica, ica_
        else:
            return X_ica


def _vector_expit(vec, zero_center=True, zero_balance=False, minmaxscale=None, standardscale=True, zerotol=1e-3, do_expit=True):
    """helper function for scipy.special.expit for normalizing vector before expit operation.

    :param vec: data vector, numpy array or sparse.
    :param zero_center: zero center (subtract mean) of vec
    :param zero_balance: weight the standard scale with number of zero entries in vec so that weight = _np.count_nonzero(vec) / vec.shape[0]
    :param minmaxscale: apply a minmax scale, tuple (min, max) values, on expit values. If None do nothing.
    :param standardscale: divide by the standard devation of vec
    :param zerotol: Values below will be rounded to zero, default 1e-3
    :returns: input type
    :rtype: array or sparse

    """

    was_sparse = False
    if _issparse(vec):
        vec = vec.toarray().flatten()
        was_sparse = True

    vm = vec.mean() if zero_center else 0
    vsd = vec.std() if standardscale else 1
    zb = (_np.count_nonzero(vec) / vec.shape[0]) if zero_balance else 1

    if zb == 0:
        return vec

    vec = (vec - vm) / (zb * vsd)

    if do_expit:
        vec = _expit(vec)
        vec[vec < zerotol] = 0

    if minmaxscale is not None:
        vec = _minmax_scale(vec, feature_range=minmaxscale)

    if was_sparse:
        vec = _csr.csr_matrix(vec)

    return vec


def expit(data, axes=0, copy=False, do_expit=True, **kwargs):
    """A wrapper function for the scipy.special.expit function to apply row or column wise on a matrix data

    :param data: anndata, numpy or sparse array
    :param axes: along columns (0) or rows (1)
    :param copy: copy the input to standalone data.
    :param vec: data vector, numpy array or sparse.
    :param zero_center: zero center (subtract mean) of vec
    :param zero_balance: weight the standard scale with number of zero entries in vec so that weight = _np.count_nonzero(vec) / vec.shape[0]
    :param minmaxscale: apply a minmax scale, tuple (min, max) values, on expit values. If None do nothing.
    :param standardscale: divide by the standard devation of vec
    :param zerotol: Values below will be rounded to zero, default 1e-3
    :param do_expit: Skip exit if false, default True. If skipped only standard normalizing is done.
    :returns: the same as input type
    :rtype: anndata, numpy or sparse array

    """

    if isinstance(data, _AnnData):
        data_is_AnnData = True
        adata = data.copy() if copy else data
        X = adata.X
    else:
        data_is_AnnData = False
        X = data

    if _issparse(X):
        if axes == 1:
            X = (x for x in X)
            X = _scs.vstack([_vector_expit(x, do_expit=do_expit, **kwargs) for x in X], format='csr')

        elif axes == 0:
            X = (x for x in X.T)
            X = _scs.vstack([_vector_expit(x, do_expit=do_expit, **kwargs) for x in X], format='csr').T

    else:
        if _issparse(X):
            X = X.A

        X = _np.apply_along_axis(_vector_expit, axes, X, do_expit=do_expit, **kwargs)

    if data_is_AnnData:
        adata.X = X
        return adata if copy else None

    else:
        return X
