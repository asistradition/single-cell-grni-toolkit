from anndata import AnnData as _AnnData
from scanpy.logging import logging as _logg
import numpy as _np
from scipy.sparse import issparse as _issparse


def robustscale(data, zero_center=True, quantile_range=(25.0, 75.0), copy=False):
    """Scale data to unit variance and zero mean.

    Parameters
    ----------
    data : :class:`~anndata.AnnData`, `np.ndarray`, `sp.sparse`
        The (annotated) data matrix of shape `n_obs` × `n_vars`. Rows correspond
        to cells and columns to genes.
    zero_center : `bool`, optional (default: `True`)
        If `False`, omit zero-centering variables, which allows to handle sparse
        input efficiently.
    copy : `bool`, optional (default: `False`)
        If an :class:`~anndata.AnnData` is passed, determines whether a copy
        is returned.

    Returns
    -------
    AnnData, None
        Depending on `copy` returns or updates `adata` with a scaled `adata.X`.
    """
    if isinstance(data, _AnnData):
        adata = data.copy() if copy else data
        # need to add the following here to make inplace logic work
        if zero_center and _issparse(adata.X):
            _logg.msg(
                '... scale_data: as `zero_center=True`, sparse input is '
                'densified and may lead to large memory consumption')
            adata.X = adata.X.toarray()
        robustscale(adata.X, zero_center=zero_center, copy=False)
        return adata if copy else None

    X = data.copy() if copy else data  # proceed with the data matrix
    zero_center = zero_center if zero_center is not None else False if _issparse(X) else True

    if zero_center and _issparse(X):
        _logg.msg('... scale_data: as `zero_center=True`, sparse input is '
                  'densified and may lead to large memory consumption, returning copy',
                  v=4)
        X = X.toarray()
        copy = True

    _rscale(X, zero_center, quantile_range=quantile_range)

    return X if copy else None


def _rscale(X, zero_center=True, quantile_range=(25.0, 75.0)):

    # TODO: Fix it so that this is robust against large data sets just as scanpy.pp.scale is for the StandardScaler.

    from sklearn.preprocessing import RobustScaler
    scaler = RobustScaler(with_centering=zero_center, copy=False, quantile_range=quantile_range).fit(X)
    # user R convention (unbiased estimator)
    scaler.transform(X)


def pick_n_comp(data, mindiff=0.01):

    if isinstance(data, _AnnData):
        explaind_variance = data.uns['pca']['variance_ratio']
    else:
        explaind_variance = data

    n_comps = _np.where(_np.diff(_np.diff(_np.cumsum(explaind_variance)) > mindiff))[0][0]

    return n_comps


def ftt(data, reversed=False, copy=False, correction=-1):
    """
    Freeman-Tukey transform (FTT), y = √(x) + √(x + 1) + correction

    reversed this is x = (y - correction)^2 - 1

    correction is default -1 to preserve sparse data.
    """

    if isinstance(data, _AnnData):
        adata = data.copy() if copy else data

        ftt(adata.X, reversed=reversed, copy=False)
        return adata if copy else None

    X = data.copy() if copy else data

    if _issparse(X):
        X.data = _np.sqrt(X.data) + _np.sqrt(X.data + 1) + correction

    else:
        nnz = _np.nonzero(X)
        X[nnz] = _np.sqrt(X[nnz]) + _np.sqrt(X[nnz] + 1) + correction

    if reversed:
        raise NotImplementedError
        X[nnz] = _np.square(X[nnz] - correction) - 1

    return X if copy else None
