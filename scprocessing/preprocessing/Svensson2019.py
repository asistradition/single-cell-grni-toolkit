# module facilitating a simple statistical overview shamelessly ripped from
# Valentine Svensson: Droplet scRNA-seq is not zero-inflated.
# doi: https://doi.org/10.1101/582064
import numpy as _np
from scipy.stats import spearmanr as _spearmanr, ks_2samp as _ks_2samp
from scipy import optimize as _optimize
from scipy.sparse import issparse as _issparse
from anndata import AnnData as _AnnData
import matplotlib.pyplot as _plt


def add_statistics(data, use_layer='X', copy=True):

    if isinstance(data, _AnnData):
        adata = data.copy() if copy else data

        if use_layer == 'X':
            # mean_, var_, frac_zero = add_statistics(adata.X, copy=False)
            mean_, var_, frac_zero, phi_hat, var_corr, zero_corr, ks_stats, ks_zero_stats = add_statistics(adata.X, copy=False)
        elif use_layer == 'raw':
            # mean_, var_, frac_zero = add_statistics(adata.raw.X, copy=False)
            mean_, var_, frac_zero, phi_hat, var_corr, zero_corr, ks_stats, ks_zero_stats = add_statistics(adata.raw.X, copy=False)
        else:
            # mean_, var_, frac_zero = add_statistics(adata.layers[use_layer], copy=False)
            mean_, var_, frac_zero, phi_hat, var_corr, zero_corr, ks_stats, ks_zero_stats = add_statistics(adata.layers[use_layer], copy=False)

        adata.var['mean_'] = mean_
        adata.var['var_'] = var_
        adata.var['frac_zero'] = frac_zero

        adata.uns['phi_hat'] = phi_hat
        adata.uns['var_corr'] = var_corr
        adata.uns['zero_corr'] = zero_corr
        adata.uns['ks_p_value'] = ks_stats
        adata.uns['ks_zero_p_value'] = ks_zero_stats

        return adata if copy else None

    X = data

    # if not _issparse(X):
    #     raise ValueError("This function only works for sparse matrices")

    if _issparse(X):
        mean_ = _np.array(X.mean(0))[0]
        var_ = _np.array(X.power(2).mean(0) - mean_ ** 2)[0]
        frac_zero = _np.array(1 - (X > 0).sum(0) / X.shape[0])[0]
    else:
        mean_ = X.mean(0)
        var_ = _np.array(_np.power(X, 2).mean(0) - mean_ ** 2)
        frac_zero = 1 - (X > 0).sum(0) / X.shape[0]

    phi_hat, __ = _optimize.curve_fit(var_fun, mean_, var_)
    var_corr = _spearmanr(var_fun(mean_, phi_hat), var_)[0]
    ks_stats = _ks_2samp(var_fun(mean_, phi_hat), var_)[1]

    zero_corr = _spearmanr(prob_zero_fun(mean_, phi_hat), frac_zero)[0]
    ks_zero_stats = _ks_2samp(prob_zero_fun(mean_, phi_hat), frac_zero)[1]

    return mean_, var_, frac_zero, phi_hat, var_corr, zero_corr, ks_stats, ks_zero_stats


def var_fun(mu, phi):
    return mu + phi * mu ** 2


def prob_zero_fun(mu, phi):
    if phi == .0:
        return _np.exp(-mu)

    phi_1 = 1. / phi
    return (phi_1 / (mu + phi_1)) ** phi_1


def stats_vs_mean(xx=None, scatter_data=None, phi_vec=[0., 0.5, 1.0, 2.0, 4.0], color='r', figsize=(10, 4), logrange=(-3, 3), colnames=['mean_', 'var_', 'frac_zero'], rho_var=None, rho_zero=None, p_var=None, p_zero=None):

    if xx is None:
        xx = _np.logspace(*logrange, num=128)

    fig = _plt.figure(None, figsize=figsize, constrained_layout=False)
    ax = fig.subplots(1, 2)

    for phi in phi_vec:
        c = color
        label = rf'NB ($\phi$ = {phi:0.2f})'

        if scatter_data is None:
            alpha = 1.5 / (1. + phi)
        else:
            alpha = 1

        if phi == 0 and scatter_data is None:
            c = 'k'
            label = 'Poisson'
            alpha = 1.0

        yy = var_fun(xx, phi)
        ax[0].loglog(xx, yy, c=c, label=label, alpha=alpha)
        ax[0].plot(xx, yy, lw=3, c='w', zorder=-10) if scatter_data is not None else None

        yy = prob_zero_fun(xx, phi)
        # ax[1].semilogx(xx, yy, c=c, label=label)
        ax[1].semilogx(xx, yy, c=c)
        ax[1].plot(xx, yy, lw=3, c='w', zorder=-10) if scatter_data is not None else None

    if scatter_data is not None:
        label0 = 'Observed RNA' if rho_var is None else rf'Observed RNA, $\rho$ = {rho_var:0.3f}'
        label0 = label0 + '' if p_var is None else label0 + '\n' + rf'KS $p$ = {p_var:0.3f}'

        ax[0].loglog(scatter_data[colnames[0]].values, scatter_data[colnames[1]].values, c='k', label=label0, rasterized=True, zorder=-100, marker='o', linestyle='None')

        label1 = 'Observed RNA' if rho_zero is None else rf'Observed RNA, $\rho_0$ = {rho_zero:0.3f}'
        label1 = label1 + '' if p_zero is None else label1 + '\n' + rf'KS $p_0$ = {p_zero:0.3f}'

        ax[1].semilogx(scatter_data[colnames[0]].values, scatter_data[colnames[2]].values, c='k', label=label1, rasterized=True, zorder=-100, marker='o', linestyle='None')

    ax[0].spines['top'].set_visible(False)
    ax[0].spines['right'].set_visible(False)

    ax[0].set_xlabel('Mean')
    ax[0].set_ylabel('Variance')
    ax[0].legend()
    if scatter_data is not None:
        ax[0].legend(scatterpoints=3)

    ax[1].spines['top'].set_visible(False)
    ax[1].spines['right'].set_visible(False)

    ax[1].set_xlabel('Mean')
    ax[1].set_ylabel('Probability of zero count')

    if rho_zero is not None:
        ax[1].legend()
        if scatter_data is not None:
            ax[1].legend(scatterpoints=3)

    fig.tight_layout()

    return fig, ax
