from scipy.sparse import issparse as _issparse
from sklearn.base import BaseEstimator as _BaseEstimator, TransformerMixin as _TransformerMixin
# from sklearn.linear_model import RANSACRegressor as _RANSACRegressor
from sklearn.linear_model import LinearRegression as _LinearRegression
from anndata import AnnData as _AnnData
import numpy as _np
import scipy as _sp
# import pandas as _pd
# import scanpy as _sc
import time as _time


class Decay(_BaseEstimator, _TransformerMixin):
    """Documentation for decay

    """
    def __init__(self,
                 regressmethod=_LinearRegression,
                 rolling_window_size=None,
                 quantile_window=100,
                 lower_quantile=0.2,
                 expression='X',
                 velocity='velocity',
                 memoryefficient=True,
                 subset_obs=None,
                 verbose=True,
                 regresskwargs={'fit_intercept': False, 'n_jobs': -1}):

        super(Decay, self).__init__()
        self.regressmethod = regressmethod
        self.rolling_window_size = rolling_window_size
        self.quantile_window = quantile_window
        self.lower_quantile = lower_quantile
        self.expression = expression
        self.velocity = velocity
        self.memoryefficient = memoryefficient
        self.subset_obs = subset_obs
        self.verbose = verbose
        self.regresskwargs = regresskwargs

    def _make_dense(self, X):

        if not self.memoryefficient and _issparse(X):
            X = X.A

        return X

    def _get_layer_data(self, data, copy=True):

        if isinstance(data, _AnnData):
            adata = data.copy() if copy else data
            if self.expression == 'X':
                X = adata.X
            elif self.expression == 'raw':
                X = adata.raw.X
            else:
                X = adata.layers[self.expression]

            Y = adata.layers[self.velocity]
        else:
            X = data.copy()
            Y = None

        X = self._make_dense(X)

        return X, Y

    def _iterate_quantile(self, y):

        qtls = _np.zeros(y.shape) * _np.nan
        halfwindow = self.quantile_window // 2
        for i in _np.arange(halfwindow, y.size - halfwindow, 1):
            ra = [i - halfwindow, i + halfwindow]
            qtls[i] = _np.quantile(y[ra[0]:ra[1]], self.lower_quantile)

        # print(type(qtls))
        # print(_np.isnan(qtls))
        qtls[_np.isnan(qtls)] = _np.nanmax(qtls)

        return qtls

    def rolling_regression(self, x, y):

        rollwindow = y.size // 10 if self.rolling_window_size is None else self.rolling_window_size
        rollwindow = rollwindow // 2
        rollwindow = max(rollwindow, 3)

        alphas = []
        for i in _np.arange(rollwindow, y.size - rollwindow - 1, 1):

            ra = [i - rollwindow, i + rollwindow]
            wx = x[ra[0]:ra[1]]
            wy = y[ra[0]:ra[1]]

            regf = self.regressmethod(**self.regresskwargs)
            regf = regf.fit(wx.reshape(-1, 1), wy)

            alphas.append(regf.coef_[0])

        return alphas

    def _iterate_feature(self, X, Y):

        tot = X.shape[0]
        negatives = Y < 0

        decays = {}
        start_time = _time.time()
        self._extime = start_time
        for i, (x, y, neg) in enumerate(zip(X, Y, negatives)):

            if self.verbose:
                print(f'Working on gene {i}/{tot} = {100*i/(tot-1):.3f}%, t = {self._extime:.3f}s', end='\r')

            if _np.sum(neg) < self.quantile_window:
                decays[i] = _sp.array([])
                self._extime = _time.time() - start_time
                continue

            x = x[neg]
            y = y[neg]

            indx = x.argsort()
            y = y[indx]
            x = x[indx]
            qtls = self._iterate_quantile(y)
            x = x[y < qtls]
            y = y[y < qtls]

            rolling_alpha = self.rolling_regression(x, y)

            decays[i] = _np.abs(_sp.array(rolling_alpha))

            self._extime = _time.time() - start_time

        return decays

    def get_best_decay(self, qt=0.95):

        opt_decay = []
        for k, v in self.decays_.items():
            if v.size == 0:
                opt_decay.append(0)
            else:
                v = v[v < _sp.quantile(v, qt)]
                opt_decay.append(v.max())

        self.opt_decay = opt_decay

    def fit(self, X, Y=None, copy=True):

        if isinstance(X, _AnnData):
            adata = X.copy() if copy else X

            X, Y = self._get_layer_data(adata)
        else:
            if Y is None:
                raise ValueError("Y needs to be set if X is a matrix")

        decays = self._iterate_feature(X.T, Y.T)
        self.decays_ = decays

        return self

    def transform(self, X, Y=None, copy=False):

        if isinstance(X, _AnnData):
            adata = X.copy() if copy else X

            self.get_best_decay()
            adata.var['decay'] = self.opt_decay

            return adata if copy else None
        else:
            self.get_best_decay()

            return self.opt_decay


def adjustPV2decay(X, velocity=None, decay=None, copy=False, l1='velocity', l2='X', l3='velocity-dX', decay_key='decay'):

    if isinstance(X, _AnnData):
        adata = X.copy() if copy else X

        if l2=='X':
            X = adata.X
        else:
            X = adata.layers[l2]

        velocity = adata.layers[l1]

        if decay_key not in adata.var_keys():
            raise ValueError(f"Can't find decay values in {decay_key}.")

        decay = adata.var['decay'].values

    else:
        if (velocity is None) or (decay is None):
            raise ValueError("Need to supply velocity and decay rates as well.")


    adjvelocity = velocity + decay*X

    return adjvelocity
