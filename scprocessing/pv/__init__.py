from scipy.sparse import issparse as _issparse
from anndata import AnnData as _AnnData
from scanpy.neighbors import Neighbors as _Neighbors
import scanpy as _sc
import scipy as _sp
import scipy.sparse as _scs
import pandas as _pd
# import warnings as _warnings
from sklearn.base import BaseEstimator as _BaseEstimator, TransformerMixin as _TransformerMixin
import sklearn.linear_model as _linear_models
import time as _time


def _matrixmult(A, B):
    """A wrapper for matrix mutiplication. Should handle most types of 2d arrays.

    :param A: A matrix
    :param B: A matrix
    :returns: A matrix
    :rtype: matrix

    """

    C = A @ B
    return C


def assign_cluster_to_indices(clusters, indices, choose=0):
    """Use a pandas categorical and a list of unique indices to map anchors to cells"""

    if not isinstance(clusters, _pd.core.arrays.categorical.Categorical):
        clusters = _pd.core.arrays.categorical.Categorical(clusters)

    cell_anchors = {}
    for i in clusters.categories.tolist():

        anchors = indices[(clusters == i).tolist()]
        choose = choose if not (choose is None) else _sp.random.randint(len(anchors))
        anchor = anchors[choose]
        anchr = _sp.where(indices == anchor)[0][0]
        cell_anchors[i] = anchr

    return cell_anchors


def reorder_before_after_dpt(data, copy=True):

    if not isinstance(data, _AnnData):
        raise ValueError(f"data must be an AnnData obejct. Currently its {type(data)}")
    else:
        adata = data.copy() if copy else data

    if 'dpt_anchors' not in adata.uns_keys():
        raise ValueError(f"Before and after dpt_anchors needs to be set in adata.uns.")

    neighbors = _Neighbors(adata)

    if not hasattr(neighbors, 'n_neighbors'):
        raise ValueError(f"neighbors needs to be computed.")

    # celli = adata.uns['iroot']

    closest_anchors = []
    closest_anchors_index = []
    for celldists in neighbors.distances_dpt:
        celldists = celldists / celldists.max()
        min_anchor_index = _sp.argmin(celldists[adata.uns['dpt_anchors']])
        closest_anchor = adata.uns['dpt_anchors'][min_anchor_index]
        # upstream_anchor = self.anchors[closest_anchor]
        closest_anchors.append(closest_anchor)
        closest_anchors_index.append(min_anchor_index)

    return closest_anchors, closest_anchors_index


class PV(_BaseEstimator, _TransformerMixin):

    def __init__(self,
                 data,
                 n_neighbors=None,
                 max_neighbors=None,
                 iterations=0,
                 regressf=_linear_models.LinearRegression,
                 kwfit={},
                 iroot=None,
                 anchors=None,
                 impute_velocity=False,
                 max_c=True,
                 use_layer='Ms',
                 add_layer='velocity',
                 offset_layer='offset',
                 predict_offset=True,
                 within_clusters=None,
                 n_comps=50,
                 n_pcs=None,
                 n_dcs=None,
                 use_rep='X_pca',
                 swap_nan=True,
                 reverse=False,
                 scale_max=True,
                 use_genes=None,
                 random_state=42,
                 memoryefficient=True,
                 verbose=False):
        """Extracts expression values and diffusion pseudotime for neighbors of a specific cell.

        :param adata: AnnData object
        :param use_genes: use only the genes in this list
        :param use_raw: use raw expression
        :param n_neighbors: try to extract this many neighbors
        :param max_c: force max n_neighbors using only the closest.
        :returns: Y, x
        :rtype: numpy.array

        """

        super(PV, self).__init__()

        self.random_state = random_state

        self.regressf = regressf

        self.use_layer = use_layer
        self.add_layer = add_layer
        self.offset_layer = offset_layer
        self.n_comps = n_comps
        self.n_pcs = n_pcs
        self.n_dcs = n_dcs
        self.use_rep = use_rep
        self.verbose = verbose
        self.max_c = max_c
        self.iroot = iroot
        self.swap_nan = swap_nan
        self.predict_offset = predict_offset
        self.use_genes = use_genes
        self.scale_max = scale_max
        self.reverse = reverse
        self.kwfit = kwfit
        self.impute_velocity = impute_velocity
        self.iterations = iterations
        self.anchors = None
        self.within_clusters = None

        self._parse_input_anndata(data, n_neighbors)
        if max_neighbors is None:
            if self.iterations:
                self.max_neighbors = _sp.inf
            else:
                self.max_neighbors = self.neighbors.n_neighbors
        else:
            self.max_neighbors = max_neighbors

        try:
            maxn = iter(self.max_neighbors)
        except TypeError:
            maxn = iter([self.max_neighbors])

        if anchors is None and self.anchors is None:
            self.anchors = {self.iroot: self.iroot}
        elif anchors is not None:
            self.anchors = anchors

        if within_clusters is not None:
            if not isinstance(within_clusters, _pd.core.arrays.categorical.Categorical):
                within_clusters = _pd.core.arrays.categorical.Categorical(within_clusters)

            self.within_clusters = within_clusters

        self.max_neighbors = [i for i in maxn]

    def _parse_input_anndata(self, data, n_neighbors):
        """
        Handle AnnData objects correctly.
        """

        if not isinstance(data, _AnnData):
            adata = _sc.AnnData(data)

        else:
            adata = data

        if ('iroot' in adata.uns_keys()) and (self.iroot is None):
            self.iroot = adata.uns['iroot'][0]
        else:
            adata.uns['iroot'] = self.iroot

        if ('anchors' in adata.uns_keys()) and (self.anchors is None):
            self.anchors = adata.uns['anchors'].copy()

        if 'pca' in data.uns_keys():
            neighbors = _Neighbors(adata)
        else:
            _sc.pp.pca(data, self.n_comps)
            neighbors = _Neighbors(adata)

        if not hasattr(neighbors, 'n_neighbors'):  # Assume nothing as been precomputed
            # _warnings.warn("neighborhood not computed, is now run with default parameters", UserWarning)
            n_default = 10 if (n_neighbors is None) else n_neighbors
            neighbors.compute_neighbors(n_neighbors=n_default, use_rep=self.use_rep, n_pcs=self.n_pcs)
            neighbors.compute_transitions()
            d_default = 15 if (self.n_dcs is None) else self.n_dcs
            neighbors.compute_eigen(n_comps=d_default)

        self.neighbors = neighbors
        self.iter_neighbours = neighbors.distances**(self.iterations + 1)

    def _make_dense(self, X):
        """Densify array if memoryefficient is set to False
        """

        if not self.memoryefficient and _issparse(X):
            X = X.A

        return X

    def _get_layer_data(self, data, copy=True):
        """
        Get specified layer from an AnnData object. If not AnnData object matrix will be returned.
        """

        if isinstance(data, _AnnData):
            adata = data.copy() if copy else data
            if self.use_layer == 'X':
                X = adata.X
            elif self.use_layer == 'raw':
                X = adata.raw.X
            else:
                X = adata.layers[self.use_layer]

        else:
            X = data.copy()

        X = self._make_dense(X)

        return X

    def _extract_expression_and_dpt(self, adata, cell, maxn=None):
        """Extracts expression values and pseudotime for neighbors of a specific cell.

        :param adata: AnnData object
        :param cell: what cell should be the center
        :returns: Y, x
        :rtype: numpy.array

        """

        if maxn is None:
            maxn = self.max_neighbors[0]

        if isinstance(cell, str):
            celli = _sp.where(adata.obs_names == cell)[0][0]
        elif isinstance(cell, int):
            celli = cell
        else:
            raise ValueError(f"cell imput must be int or str. Currently its {type(cell)}")

        neighbor_indices = self._get_neighbors(celli)

        x, neighbor_indices = self._get_neighbor_distances(neighbor_indices, celli, maxn=maxn)

        Y = self._extract_neighbor_data(adata, neighbor_indices)

        return Y, x

    def _get_neighbors(self, i):
        """Get neighbor indices for cell i.

        :param adata: AnnData object
        :param i: cell of interest
        :param get_distances: also get the raw distances to neighbors. Not implemented yet.
        :returns: neighbor indices
        :rtype: numpy array
        """

        neighbor_indices = self.iter_neighbours[i, :]
        neighbor_indices = neighbor_indices.sum(0).nonzero()
        neighbor_indices = _sp.unique(_sp.append(i, neighbor_indices[1]))

        return neighbor_indices

    def _extract_neighbor_data(self, data, neighbor_indices, use_var=None):
        """
        Get neighbor data from array.
        """

        adata = data.raw if (self.use_layer == 'raw') else data

        use_variable = False
        if use_var is not None:
            use_variable = True
            variables = [i for i in use_var if i in adata.var_names]

        adata_comp = adata[:, variables] if use_variable else adata

        if self.use_layer == 'X' or self.use_layer == 'raw':
            X = adata_comp[neighbor_indices, :].X.copy()
        else:
            X = adata_comp[neighbor_indices, :].layers[self.use_layer].copy()

        return X

    def _gene_rate(self, Y, x, center_cell=None):
        """Calculate the rate of change for each gene at each cell given it's neighboring cells.

        :param Y: gene expression, dimension (samples) x (genes)
        :param x: time points, dimension (samples)
        :regressf: function object, sklearn _linear_models.LinearRegression by default. Should accept two parameters (expr, time) as input.
        :inpute_velocity: regress velocity when gene expression for center gene = 0. Bool, default False.
        :center_cell: if supplied will be the index of the time vector where the center cell is assigned, default None. where delta t==0
        :returns: vel, offset
        :rtype: float

        """

        if center_cell is None:
            center_cell = _sp.where(x == 0)[0]
            if len(center_cell) > 1:
                center_cell = center_cell[0]  # It turns out cells can have the exact same diffusion time and be neighbors, meaning that they produce twins in the data.

        was_sparse = False
        if _issparse(Y):
            Y = Y.toarray()
            was_sparse = True

        Ycc = Y[center_cell, :]
        # xcc = x[center_cell, :]
        where_not_zero = _sp.array((Ycc != 0).astype(int)).squeeze()

        reg_fit = self.regressf(**self.kwfit)

        x = x.reshape(-1, 1)

        if self.predict_offset:
            select = _sp.in1d(range(x.shape[0]), center_cell)
            x = x[~select]
            Y = Y[~select, :]

        fitted = False
        try:
            reg_fit = reg_fit.fit(x, Y)
            if hasattr(reg_fit, 'estimator_'):
                reg_fit = reg_fit.estimator_
            fitted = True
        except SystemError:
            fitted = False

        if fitted:
            offset = reg_fit.intercept_
            vel = reg_fit.coef_.reshape(-1, )
        else:
            offset = _sp.zeros(Ycc.shape).reshape(-1, )
            vel = _sp.zeros(Ycc.shape).reshape(-1, )

        # se = _sp.square(Ycc - offset).sum(0).reshape(-1, )
        # me = _sp.square(Ycc).sum(0).reshape(-1, )
        se, me = self._predict(x, Y, vel, offset, Ycc=Ycc if self.predict_offset else None)

        if not self.impute_velocity:
            offset = offset * where_not_zero
            vel = vel * where_not_zero
            se = se * where_not_zero
            me = me * where_not_zero

        if was_sparse or self.impute_velocity:
            return _scs.csr.csr_matrix(vel), _scs.csr.csr_matrix(offset), _scs.csr.csr_matrix(se), _scs.csr.csr_matrix(me)
        else:
            return vel, offset, se, me

    def _predict(self, x, Y, vel, offset, Ycc=None):
        """
        Generate the predicted data and calculate standard error
        """

        # offset = offset if (Ycc is None) else Ycc
        # Ypred = x * vel + offset
        # se = _sp.square(Ypred - Y).sum(0).reshape(-1, )
        # me = _sp.square(Y - Y.mean(0)).sum(0).reshape(-1, )

        Ycc = _sp.zeros(offset.shape) if (Ycc is None) else Ycc
        se = _sp.square(Ycc - offset).sum(0).reshape(-1, )
        me = _sp.square(Ycc - Ycc.mean()).sum(0).reshape(-1, )

        return se, me

    def _find_anchor_cell(self, celli):
        """Find the upstream closest anchor to a specfic cell and return
        index of as well as annotation for downstream leaf node.

        """
        leafs = list(self.anchors.keys())

        if self.within_clusters is None:
            celldists = self.neighbors.distances_dpt[celli]
            celldists = celldists / celldists.max()
            min_leaf_index = _sp.argmin(celldists[leafs])  # Find closest leaf
            closest_leaf = leafs[min_leaf_index]  # Extract closest leaf cell
        else:  # TODO: Instead of using distances use clusters to estimate anchor and leaf.
            cluster = self.within_clusters[celli]
            leafclusters = self.within_clusters[leafs]
            li = leafclusters.isin([cluster])
            closest_leaf = _sp.array(leafs)[li][0]
            min_leaf_index = leafclusters[li][0]

        upstream_dpt_root = self.anchors[closest_leaf]  # extract upstream root

        return upstream_dpt_root, closest_leaf, min_leaf_index

    def _get_neighbor_distances(self, neighbor_indices, celli, maxn=None):
        """Get the time order of cells. Cell delta times are extracted
        from the central cell while ordering is derived from the anchor cell.
        """

        if maxn is None:
            maxn = self.max_neighbors[0]

        iroot = self._find_anchor_cell(celli)[0]

        t0 = self.neighbors.distances_dpt[iroot]

        t1 = self.neighbors.distances_dpt[celli]

        if self.scale_max:
            t0 = t0 / t0.max()
            t1 = t1 / t1.max()

        if self.reverse:
            deltat = t0[celli] - t0
        else:
            deltat = t0 - t0[celli]

        t1 = t1 * _sp.sign(deltat)

        delta_times = t1[neighbor_indices]

        if self.max_c:
            closest_indices = _sp.argsort(_sp.absolute(delta_times))[:min(maxn, len(neighbor_indices))]
            delta_times = delta_times[closest_indices]
            neighbor_indices = neighbor_indices[closest_indices]

        return (delta_times, neighbor_indices)

    def _optimize(self, adata, cell):

        curr_err = 0
        opt_delta = _sp.inf
        cellvals = {}
        for maxn in self.max_neighbors:
            Y, x = self._extract_expression_and_dpt(adata, cell, maxn=maxn)

            cellvals = self._gene_rate(Y, x)
            err = _sp.sum(cellvals[2]) / (1 if _sp.sum(cellvals[3]) == 0 else _sp.sum(cellvals[3]))
            err = 1 if ~_sp.isfinite(err) else err
            curr_delta = _sp.absolute(curr_err - err)
            # print(curr_delta, opt_delta, maxn, opt_delta > curr_delta)
            # print(old_delta, curr_delta)

            # if min_err > _sp.sum(cellvals[2]) / cellvals[2].shape[0]:
            if opt_delta > curr_delta:

                # min_err = _sp.sum(cellvals[2]) / cellvals[2].shape[0]
                opt_val = cellvals
                if _sp.isinf(maxn):
                    optn = Y.shape[0]
                else:
                    optn = maxn

                opt_delta = curr_delta

            curr_err = err

        return (*opt_val, optn)

    def fit(self, data, copy=False):

        if isinstance(data, _AnnData):
            adata = data.copy() if copy else data

        else:
            adata = _sc.AnnData(data)

        velobs = {}
        velobs['cell'] = []
        velobs[self.add_layer] = []
        velobs[self.offset_layer] = []
        velobs['se'] = []
        velobs['me'] = []
        velobs['optimal'] = []

        start_time = _time.time()
        self._extime = start_time

        for i, cell in enumerate(adata.obs_names):
            if self.verbose:
                print(f'Working on cell {cell} = {i}/{adata.obs_names.shape[0]} = {100*i/(adata.obs_names.shape[0]-1):.3f}%, t = {self._extime:.3f}s', end='\r')

            cellvals = self._optimize(adata, cell)

            velobs['cell'].append(cell)
            velobs[self.add_layer].append(cellvals[0])
            velobs[self.offset_layer].append(cellvals[1])
            velobs['se'].append(cellvals[2])
            velobs['me'].append(cellvals[3])
            velobs['optimal'].append(cellvals[4])

            self._extime = _time.time() - start_time

        if self.verbose:
            print(f'Working on cell {cell} = {i}/{adata.obs_names.shape[0]} = {100*i/(adata.obs_names.shape[0]-1):.3f}%, t = {self._extime:.3f}s')

        self.velobs = velobs
        return self

    def transform(self, data, copy=True):

        was_anndata = False
        if isinstance(data, _AnnData):
            was_anndata = True
            adata = data.copy() if copy else data

        else:
            adata = _sc.AnnData(data)

        for k, v in self.velobs.items():
            if k not in ['cell', 'optimal']:
                if _issparse(self.velobs[k][0]):
                    adata.layers[k] = _scs.vstack(v, format='csr')
                else:
                    adata.layers[k] = _sp.array(v)

            elif k in ['optimal']:
                adata.obs['optimal_velocity_neighbours'] = v

        if was_anndata:
            return adata if copy else None
        else:
            return adata
