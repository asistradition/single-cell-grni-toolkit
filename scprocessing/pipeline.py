import pandas as _pd
import numpy as _np
import scanpy.api as _sc
import scprocessing.plotting.anndata as _scpl
import warnings


def base_computations(data, npcs=50, nneighbors=10, resolution=1, spread=1, keyX_added='', min_dist=0.1, random_state=42, recompute_pca=True, metric='euclidean', use_highly_variable=False, metric_kwds={}, compute_tsne=False, compute_louvain=False, svd_solver='arpack'):

    if recompute_pca:
        if 'X_pca' in data.obsm_keys():
            del data.obsm['X_pca']
        if 'pca' in data.uns_keys():
            del data.uns['pca']
        # if 'PCs' in data.varm_keys():
        #     del data.varm['PCs']

    if 'X_diffmap' in data.obsm_keys():
        del data.obsm['X_diffmap']
    # if 'PCs' in data.varm_keys():
    #     del data.varm['PCs']

    if recompute_pca or 'X_pca' not in data.obsm_keys():
        _sc.tl.pca(data, svd_solver=svd_solver, n_comps=min(max(npcs, 50), data.n_obs - 1), use_highly_variable=use_highly_variable, random_state=random_state)

    if 'neighbors' in data.uns_keys():
        del data.uns['neighbors']

    _sc.pp.neighbors(data, n_neighbors=nneighbors, metric=metric, metric_kwds=metric_kwds, n_pcs=npcs, random_state=random_state)

    if compute_tsne:
        if 'X_tsne' in data.obsm_keys():
            del data.obsm['X_tsne']

        _sc.tl.tsne(data, n_pcs=npcs, random_state=random_state)

    _sc.tl.umap(data, spread=spread, min_dist=min_dist, random_state=random_state)

    if compute_louvain:
        if 'louvain' + keyX_added in data.obs_keys():
            del data.obs['louvain']
        if 'louvain' + keyX_added + '_colors' in data.uns_keys():
            del data.uns['louvain' + keyX_added + '_colors']
        _sc.tl.louvain(data, resolution=resolution, random_state=random_state, key_added='louvain' + keyX_added)

    if 'leiden' + keyX_added in data.obs_keys():
        del data.obs['leiden']
    if 'leiden' + keyX_added + '_colors' in data.uns_keys():
        del data.uns['leiden' + keyX_added + '_colors']

    _sc.tl.leiden(data, restrict_to=None, resolution=resolution, random_state=random_state, key_added='leiden' + keyX_added)

    _sc.tl.diffmap(data)


def rank_genes_groups(data, groupby='leiden', keyX_added='', n_genes=100, method='t-test_overestim_var', use_raw=True, rankby_abs=False):

    if 'rank_genes_groups' in data.uns_keys():
        del data.uns['rank_genes_groups']

    if groupby == 'louvain':
        try:
            _sc.tl.rank_genes_groups(data, groupby='louvain' + keyX_added, method=method, n_genes=n_genes, use_raw=use_raw, rankby_abs=rankby_abs)
        except:
            warnings.warn("rank genes failed. Probably due to no significant genes found in the group")
    elif groupby == 'leiden':
        try:
            _sc.tl.rank_genes_groups(data, groupby='leiden' + keyX_added, method=method, n_genes=n_genes, use_raw=use_raw, rankby_abs=rankby_abs)
        except:
            warnings.warn("rank genes failed. Probably due to no significant genes found in the group")


def add_regress_pcs(data, components=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10], obsm='X_pca', key='PC'):

    for i, c in enumerate(components):
        pc = data.obsm[obsm][:, i]
        data.obs[key + str(c)] = pc


def subset_worker(data, subset, obs='louvain', npcs=50, nneighbors=10, n_genes=100, use_raw=True, resolution=1, spread=1, method='t-test_overestim_var', groupby='louvain', keyX_added='_sub'):

    selection = data.obs[obs].isin([subset])
    adata = data[selection, :].copy()
    adata.X = _sc.pp.scale(adata.X.T, zero_center=True, copy=True).T
    _sc.pp.filter_genes(adata, min_cells=10)
    del adata.uns['rank_genes_groups']

    base_computations(adata, npcs=npcs, nneighbors=nneighbors, n_genes=n_genes, use_raw=use_raw, resolution=resolution, spread=spread, method=method, groupby=groupby)

    return adata


def format_rank_gene_table(data):

    nonstattest = ['logreg']
    isstatistic = (data.uns['rank_genes_groups']['params']['method'] not in nonstattest)
    gene_rank_table = []
    for gr in data.uns['rank_genes_groups']['names'].dtype.names:

        if isstatistic:
            __ = _pd.DataFrame.from_dict({k: [i for i in data.uns['rank_genes_groups'][k][gr]] for k in ['names', 'scores', 'pvals_adj']})
        else:
            __ = _pd.DataFrame.from_dict({k: [i for i in data.uns['rank_genes_groups'][k][gr]] for k in ['names', 'scores']})

        __['comparison'] = gr + data.uns['rank_genes_groups']['params']['reference']

        gene_rank_table.append(__)

    gene_rank_table = _pd.concat(gene_rank_table)

    gene_rank_table.index.name = 'rank'

    if isstatistic:
        gene_rank_table = gene_rank_table[gene_rank_table['pvals_adj'] < 1e-3]

    return gene_rank_table


def results(data, pltsubsets=['hpf', 'louvain'], n_genes=5, representations={'umap'}, markergenes=None, foldfig=3, cmap='viridis', figsize1=(10, 6), figsize2=(12, 12), fdir=None, order='C', **kwargs):

    fig_var, __, __ = _scpl.plot_explained_variance(data)

    fig_subsetscatter, __, __ = _scpl.visualize_cell_scatter(data, pltsubsets, representations=representations, figsize=figsize1, order='F')

    subset_ranks = format_rank_gene_table(data)
    tab = subset_ranks[['comparison', 'names']].pivot(columns='comparison').head(20)['names']

    if markergenes is None:
        markergenes = [mg for mg in tab.head(n_genes).replace(_np.nan, "").T.values.flatten() if mg != ""]

    fig_genescatter, __, __ = _scpl.visualize_cell_scatter(data, markergenes, foldfig=foldfig, representations=representations, cmap=cmap, figsize=figsize2, order=order, **kwargs)

    print_files = []
    if fdir is not None:
        fnames = _scpl.save_figure(fig_var, fdir, fname='pca_variance_')
        print_files.append("[[file:" + fnames[0] + "]]")

        fnames = _scpl.save_figure(fig_subsetscatter, fdir, fname='clustering_')
        print_files.append("[[file:" + fnames[0] + "]]")

        fnames = _scpl.save_figure(fig_genescatter, fdir, fname='gene_clustering_')
        print_files.append("[[file:" + fnames[0] + "]]")

    return tab, print_files
