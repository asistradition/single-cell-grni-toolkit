import os as _os
from itertools import product as _product
import matplotlib.pyplot as _plt
import matplotlib as _mpl
# import scanpy.plotting as _scp
import scanpy as _sc
import seaborn as _sns
import numpy as _np
# from matplotlib import rcParams as _rcParams
# from scanpy.utils import doc_params as _doc_params
from scanpy.plotting._docs import doc_adata_color_etc as _doc_adata_color_etc, doc_edges_arrows as _doc_edges_arrows, doc_show_save_ax as _doc_show_save_ax
# from scanpy.plotting._tools.scatterplots import plot_scatter as _plot_scatter
import scipy.sparse as _scs
from anndata import AnnData as _AnnData


_figargs = {'nrows': 1, 'ncols': 1,
            "figsize": (8, 6), "dpi": 100,
            "facecolor": 'w', "edgecolor": 'k',
            "sharex": False, "sharey": False,
            'title': '', 'xlabel': '', 'ylabel': '',
            "savefig": False, "figname": None,
            "constrained_layout": True,
            'size': None,
            'color_map': None,
            'color': None,
            'aspect_ratio': 'auto',
            'show': False,
            'markerscale': 3}

# @_doc_params(adata_color_etc=_doc_adata_color_etc, edges_arrows=_doc_edges_arrows, show_save_ax=_doc_show_save_ax)
# def ica(adata, **kwargs):
#     """\
#     Scatter plot in ICA basis.

#     Parameters
#     ----------
#     {adata_color_etc}
#     {edges_arrows}
#     {scatter_bulk}
#     {show_save_ax}

#     Returns
#     -------
#     If `show==False` a `matplotlib.Axis` or a list of it.
#     """
#     return _plot_scatter(adata, basis='ica', **kwargs)


def save_figure(fig, fdir, fname='', pixel=True, vector=True, dpi=150, bbox_inches='tight'):

    if not _os.path.exists(fdir):
        _os.makedirs(fdir, exist_ok=True)

    figf = []
    if pixel:
        f = _os.path.join(fdir, fname + "figure.png")
        fig.savefig(f, transparent=False, dpi=dpi, bbox_inches=bbox_inches)
        figf.append(f)
    if vector:
        f = _os.path.join(fdir, fname + "figure.svg")
        fig.savefig(f, transparent=False, bbox_inches=bbox_inches)
        figf.append(f)

    return figf


def visualize_cell_scatter(adata, observations, representations={'umap', 'tsne'}, use_raw=None, foldfig=False, loc='best', order='C', fdir='/tmp', components=['1,2'], cmap='viridis', legend_loc='right margin', size=None, scatterargs={}, **kwargs):

    import logging
    logging.basicConfig(filename='/tmp/scprocessing.log', level=logging.DEBUG)  # to supress matplotlib legend label warning

    global _figargs
    figargs = {**_figargs, **kwargs}

    if isinstance(observations, str):
        observations = [observations]

    if isinstance(components, str):
        components = [components]

    if order == 'F':
        cols = len(observations) * len(components)
        rows = len(representations)
    else:
        cols = len(representations)
        rows = len(observations) * len(components)

    fig = _plt.figure(figsize=figargs['figsize'], dpi=figargs['dpi'], facecolor=figargs["facecolor"], edgecolor=figargs['edgecolor'])

    if foldfig > 0:
        if order == 'F':
            cols = foldfig
            rows = len(list(_product(observations, representations, components))) / foldfig
            rows = int(rows)
        else:
            rows = foldfig
            cols = len(list(_product(observations, representations, components))) / foldfig
            cols = int(cols)

        ax = fig.subplots(nrows=rows, ncols=cols, sharex=figargs['sharex'], sharey=figargs['sharey'])

    else:
        ax = fig.subplots(nrows=rows, ncols=cols, sharex=figargs['sharex'], sharey=figargs['sharey'])

    if isinstance(ax, _np.ndarray):
        ax = ax.flatten(order=order)
    else:
        ax = _np.array([ax])

    for i, (g, reps, c) in enumerate(_product(observations, representations, components)):

        plotfunc = getattr(_sc.pl, reps)

        if reps == 'paga':
            plax = plotfunc(adata, color=g, cmap=cmap, show=False, ax=ax[i], use_raw=use_raw, **scatterargs)
        else:
            plax = plotfunc(adata, color=g, color_map=cmap, show=False, ax=ax[i], use_raw=use_raw, components=c, legend_loc=legend_loc, size=size, **scatterargs)
        ax[i].legend(title=g, loc=loc)
        ax[i].set_title('')

    fig.tight_layout()

    figurenames = None
    figname = figargs['figname']
    if figargs['savefig']:
        if figname is None:
            fname = ""
        else:
            fname = figname

        figurenames = save_figure(fig, fdir, fname=fname)

    return fig, ax, figurenames


def plot_explained_variance(adata, hline=None, figsize=(12, 6), fdir='/tmp', **kwargs):
    """Visualize Explained variance for a scanpy dataset where sc.tl.pca has been run.

    :param adata: Anndata object.
    :param hline: marker value for horizontial line in cumulative variance plot.
    :param figsize: figure size.
    :param fdir: directory where the figure should be saved.
    :returns: (fig, ax ,figurnames)
    :rtype: (figure, axes, string)

    """

    global _figargs
    figargs = {**_figargs, **kwargs}

    fig, (ax, ax2) = _plt.subplots(nrows=1, ncols=2, figsize=figsize, dpi=figargs['dpi'], facecolor=figargs['facecolor'], edgecolor=figargs['edgecolor'])

    cumsum = _np.cumsum(adata.uns['pca']["variance_ratio"])
    __ = ax.plot(cumsum, '.')
    xl = ax.get_xlim()
    if hline is not None:
        components_below = sum(cumsum < hline)
        ax.hlines(hline, *xl, linestyles='--')

    ax.set_xlim(*xl)
    ax.grid()
    if hline is not None:
        ax.set_title(f"Cumulative sum PCA EV ratio,\n {components_below} components explain {hline*100}%.")
    else:
        ax.set_title("Cumulative sum PCA EV ratio")

    __ = ax2.plot(adata.uns['pca']["variance_ratio"], '.')
    ax2.grid()
    ax2.set_title("PCA EV ratio")

    fig.tight_layout()

    ax = (ax, ax2)

    figurenames = None
    figname = figargs['figname']
    if figargs['savefig']:
        if figname is None:
            fname = ""
        else:
            fname = figname

        fig.tight_layout()

        figurenames = save_figure(fig, fdir, fname=fname)

    return fig, ax, figurenames


def plot_nnz_expr_distribution(data, fdir='/tmp', figsize=(12, 6), hist_kws={"alpha": 1}, kde_kws={"color": "k"}, dist='norm', fit=True, ymin=-0.1, ymax=None, plot_median=True, norm_hist=True, xlabel='density', **kwargs):

    from scipy import stats
    global _figargs
    figargs = {**_figargs, **kwargs}

    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)  # currently needed for sns.distplot

    if isinstance(data, _AnnData):
        data_is_AnnData = True
        X = data.X
    else:
        X = data

    if _scs.issparse(X):
        elements = X.nonzero()
        expr = X.data
    else:
        elements = _np.nonzero(X)
        expr = X[elements[0], elements[1]]

    fig = _plt.figure(figsize=figsize, dpi=figargs['dpi'], facecolor=figargs['facecolor'], edgecolor=figargs['edgecolor'])

    ax = fig.add_subplot(1, 2, 2)
    _sns.distplot(expr, ax=ax, vertical=True, kde_kws=kde_kws, hist_kws=hist_kws, norm_hist=norm_hist)
    ax.grid()
    axl = ax.get_ylim()
    if ymin is None:
        ymin = axl[0]
    if ymax is None:
        ymax = axl[1]

    axxl = ax.get_xlim()
    median = _np.median(expr)
    ax.hlines(median, *axxl, linestyle='--')

    ax.set_ylim((ymin, ymax))
    ax.set_xlim(axxl)
    ax.set_title('KDE')
    ax.set_xlabel(xlabel)

    ax2 = fig.add_subplot(1, 2, 1, sharey=ax)
    res = stats.probplot(_np.random.choice(expr, size=min(len(expr), int(1e4)), replace=False), dist=dist, fit=fit, plot=ax2)
    ax2.grid()

    axxl = ax2.get_xlim()
    median = _np.median(expr)
    ax2.hlines(median, *axxl, linestyle='--')
    ax2.set_xlim(axxl)
    ax2.text(axxl[0] + _np.abs(axxl[0] * 0.01), median + _np.abs(median * 0.01), f"median = {median:.2f}", fontsize=12)

    fig.tight_layout()

    figurenames = None
    figname = figargs['figname']
    if figargs['savefig']:
        if figname is None:
            fname = ""
        else:
            fname = figname

        fig.tight_layout()

        figurenames = save_figure(fig, fdir, fname=fname)

    return fig, ax, figurenames


def add_genes_to_scatter(ax, labels, indices, data):

    xdat = _np.array([])
    ydat = _np.array([])
    for i, l in enumerate(labels):
        if l in indices:
            ind = indices.get_loc(l)
            xdat = _np.append(xdat, data[ind][0])
            ydat = _np.append(ydat, data[ind][1])

    labels = labels[xdat.argsort()]
    ydat = ydat[xdat.argsort()]
    ll = len(labels)
    uc = 0
    lc = 0
    for i, l in enumerate(labels):
        if l in indices:
            ind = indices.get_loc(l)
            x = data[ind][0]
            y = data[ind][1]
            r = _np.random.randint(2)
            yloc = 0.98 if (y > _np.median(ydat)) else 0.02

            uc = (uc + 1) if yloc > 0.5 else uc
            lc = (lc + 1) if yloc < 0.5 else lc
            # yloc = yloc - 0.02 if r else yloc + 0.02
            yloc = yloc - (uc % 2) * 0.03 if yloc > 0.5 else yloc + (lc % 2) * 0.03
            va = "top" if yloc > 0.5 else "bottom"
            xloc = i / ll
            textloc = (xloc, yloc)
            ax.annotate(l.split("_")[-1], xy=(x, y),
                        size=7,
                        xycoords="data",
                        xytext=textloc,
                        va=va, ha="left",
                        textcoords='axes fraction',
                        bbox=dict(boxstyle="round", fc="1"),
                        arrowprops=dict(facecolor='#d3d3d3', arrowstyle="->"))


def add_decay_line(adata, g, ax, decay_key='decay', l_style='k--', linewidth=1):

    xl = ax.get_xlim()
    xl = _np.array(xl)
    xl[0] = 0
    yl = ax.get_ylim()

    lam = adata.var[decay_key][g]
    lam = lam if lam < 0 else -lam

    ax.plot(xl, xl * lam, l_style, lw=linewidth, label=rf'$\lambda \approx {lam:.2g}$')

    ax.set_xlim(*xl)
    ax.set_ylim(*yl)


def versus(adata, x, y, layers='X', ax=None, xticks=None, x0=True, y0=False, use_raw=False, num_ticks=5, figsize=(6, 3), lfit=False, grid='x', scatterargs={}, **kwargs):

    import logging
    logging.basicConfig(filename='/tmp/scprocessing.log', level=logging.DEBUG)  # to supress matplotlib legend label warning

    global _figargs
    figargs = {**_figargs, **kwargs}

    # import matplotlib as mpl
    # import matplotlib.pyplot as plt
    from scipy.stats import linregress

    if ax is None:
        fig = _plt.figure(figsize=figsize, dpi=figargs['dpi'], facecolor=figargs['facecolor'], edgecolor=figargs['edgecolor'], constrained_layout=figargs['constrained_layout'])
        ax = fig.add_subplot(1, 1, 1)
    else:
        fig = None

    axs = _sc.pl.scatter(adata, x=x, y=y, layers=layers, use_raw=use_raw, ax=ax, size=figargs['size'], color_map=figargs['color_map'], color=figargs['color'], show=figargs['show'], **scatterargs)

    corrdata = _np.row_stack([_np.array(data.get_offsets()) for data in ax.collections])
    corrdata = corrdata[~_np.any(_np.isnan(corrdata), axis=1), :]
    corrdata = linregress(corrdata)
    xl = list(ax.get_xlim())
    yl = ax.get_ylim()
    xl[0] = min(0, xl[0])

    if x0:
        ax.hlines(0, *xl, linestyle='--', zorder=-1)

        ax.set_xlim(*xl)
    if y0:
        ax.vlines(0, *yl, linestyle='--', zorder=-1)
        ax.set_ylim(*yl)

    if lfit:
        x_vals = _np.array(xl)
        y_vals = corrdata.intercept + corrdata.slope * x_vals
        ax.plot(x_vals, y_vals, '-.k', zorder=-500)

        ax.legend(title=f'R = {corrdata.rvalue:.2f}\np = {corrdata.pvalue:.2g}')

    ax.set_ylabel(y)
    ax.set_title(figargs['title'])
    ax.set_xlabel(x)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    if grid:
        ax.grid(axis=grid, zorder=-100)

    if lfit:
        ax.legend(title=f'R = {corrdata.rvalue:.2f}\np = {corrdata.pvalue:.2g}', markerscale=figargs['markerscale'], loc='center left', bbox_to_anchor=(1, 0.5))
    else:
        ax.legend(markerscale=figargs['markerscale'], loc='center left', bbox_to_anchor=(1, 0.5))

    if xticks is not None:
        timeticks = adata.obs[xticks].sort_values().values
        xt = ax.get_xticks()
        new_labels = [f"{timeticks[int(i)]:.2f}" if ((i >= 0) and (i < len(timeticks))) else '' for i in xt]
        ax.set_xticklabels(new_labels)
        # ax.xaxis.set_major_locator(mpl.ticker.MaxNLocator(num_ticks))
        # plt.xticks(ax.get_xticks()[1:-1], [f"{i:.3g}" for i in timeticks[ax.get_xticks()[1:-1].astype(int)]], rotation='vertical')

    ax.set_aspect(figargs['aspect_ratio'])

    return fig, ax
