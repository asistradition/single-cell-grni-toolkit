from anndata import AnnData as _AnnData


def convert_raw_to_adata(data):

    adata = data.copy()
    newdata = _AnnData(adata.raw.X, obs=adata.obs, var=adata.raw.var, uns=adata.uns, obsm=adata.obsm)
    newdata.raw = newdata.copy()

    return newdata
