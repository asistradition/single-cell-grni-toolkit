import pandas as _pd
import os as _os
from glob import glob as _glob


def load_E_GEOD_38313(loadpath=_os.path.join("data", "E_GEOD_38313")):

    exprfiles = _glob(_os.path.join(loadpath, "GSM", "*"))

    datafiles = []
    for f in exprfiles:

        __ = _pd.read_table(f)

        __.columns = ["ID_REF", f.split("/")[-1].split("_")[0]]
        __ = __.set_index("ID_REF")
        datafiles.append(__)

    datafiles = _pd.concat(datafiles, 1)

    ann = load_E_GEOD_38313_annotation(loadpath=loadpath)

    datafiles = datafiles.T.merge(ann[["Source Name", "Setup"]], right_on='Source Name', left_index=True).set_index(["Setup", "Source Name"]).T

    return datafiles


def load_E_GEOD_38313_annotation(loadpath=_os.path.join("data", "E_GEOD_38313"), fname="E-GEOD-38313.sdrf.tsv"):

    ann = _pd.read_table(_os.path.join(loadpath, fname))

    ann['Source Name'] = [i.split(" ")[0] for i in ann['Source Name']]

    ann["Setup"] = [i.split("-")[-1].split("_")[0].upper().replace("WHOLE", "whole").replace("ETSW", "EtsW").replace("FGFRDN", "FGFRdn") for i in ann['Array Data File']]

    return ann


def load_(loadpath=_os.path.join("data", "E_GEOD_54746")):

    
