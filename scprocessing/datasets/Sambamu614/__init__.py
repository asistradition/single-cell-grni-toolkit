import os as _os
import pandas as _pd
from ..utils import add_annotation_to_data_var, get_ciona_genenames
from glob import glob

_datadir = "./data/Sambamu614"


def load_Sambamu614(cf=_datadir):

    import scanpy.api as sc

    adata = sc.read_10x_mtx(cf)

    adata.var.columns = ['Gene ID']
    genenames = get_ciona_genenames()

    add_annotation_to_data_var(adata, genenames)

    adata.obs.index.name = "barcode"
    # adata.obs.set_index("barcode", inplace=True, drop=False)
    # adata.obs.columns = ["cellid", "hpf"]
    # adata.obs["hpf"] = adata.obs["hpf"].astype('category')

    return adata


def load_sbc(filepattern="data/Sambamu614/SBC0*", barcode_dir="data/Sambamu614"):

    files = glob(filepattern)

    barcodes = _pd.read_csv(_os.path.join(barcode_dir, 'barcodes.tsv'), header=None)
    barcodes.columns = ['barcode']
    barcodes['cleaned'] = barcodes['barcode'].apply(lambda x: x.replace('-1', ''))

    sbc_filtered = []
    for f in files:

        sbc_gene = _os.path.split(f)[-1].split(".")[0]
        sbc = _pd.read_csv(f, sep="\t")
        sbc['umi'] = sbc['cbc'].apply(lambda x: x[16:])
        sbc['cbc'] = sbc['cbc'].apply(lambda x: x[:16])
        sbc['sbc_gene'] = sbc_gene
        del sbc['title']
        # sbc = sbc.replace(['sbc', 'cbc'], [np.nan, np.nan]).dropna()
        sbc_filtered.append(sbc[sbc['cbc'].isin(barcodes['cleaned'])].copy())

    return sbc_filtered
