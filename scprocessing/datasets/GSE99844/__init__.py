import pandas as _pd
from ..utils import add_annotation_to_data_var, get_ciona_genenames

_datasetfile = "./data/GSE99846/GSE99844/GSE99844_combined_stringtie.loom"


def load_GSE99846(lf=_datasetfile):

    import scanpy as sc

    adata = sc.read_loom(lf)
    genenames = get_ciona_genenames()
    # genenames = load_gene_annotations()

    add_annotation_to_data_var(adata, genenames)

    adata.obs.set_index("barcode", inplace=True, drop=False)
    adata.obs.columns = ["cellid", "hpf"]
    adata.obs["hpf"] = adata.obs["hpf"].astype('category')

    return adata


def load_GSE99846_velocyto():

    import scanpy.api as sc
    import scvelo as _scv
    genenames = get_ciona_genenames()

    vf = "./data/GSE99846/GSE99844/GSE99844_combined_velocyto.loom"
    avel = sc.read_loom(vf)
    avel.obs_names = [o.split(":")[0][:-3] for o in avel.obs_names]
    __ = avel.var.merge(genenames, right_on="Gene ID", left_on='Accession', how="left")
    del __['Accession']
    avel.var = __.copy()
    avel.var.set_index("Gene Name", inplace=True)
    _scv.pp.filter_and_normalize(avel)

    return avel


def add_velocyto_layers2anndata(adata, avel, copy=True):

    adata = adata.copy() if copy else adata
    var_intersection = adata.var_names.intersection(avel.var_names)
    var_intersection = var_intersection[~var_intersection.isna()]
    obs_intersection = adata.obs_names.intersection(avel.obs_names)

    adata = adata[obs_intersection, :]
    adata = adata[:, var_intersection]

    avel = avel[obs_intersection, :]
    avel = avel[:, var_intersection]

    for l in avel.layers.keys():

        adata.layers[l] = avel.layers[l].copy()

    return adata if copy else None


def flip_X_and_layer(adata, layer, xname='preX'):

    adata.layers[xname] = adata.X.copy()
    adata.X = adata.layers[layer].copy()


def add_alignment_annotations(adata, alignstatsfile="data/GSE99846/GSE99844/GSE99844_combined_STAR_stats.tsv.gz", countlayer='FPKM'):

    alignstats = _pd.read_csv(alignstatsfile, index_col=0, sep='\t')

    alignstats.index.name = "barcode"
    adata.obs = adata.obs.merge(alignstats, right_index=True, left_on="barcode", how="left")
    adata.obs['n_counts'] = adata.layers[countlayer].sum(axis=1).A1
    adata.obs['mean_coverage'] = adata.layers['Coverage'].mean(axis=1).A1
