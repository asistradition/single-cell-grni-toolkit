import pandas as _pd
import numpy as _np

genenamefile = "../data/genomic_resources/KH2013-UniqueNAME_def.txt"
kh_ky_mapping = "../data/genomic_resources/KH_KY.tsv"
geneannotationf = "../data/genomic_resources/IPRO.csv"
signaling_gene_file = '../data/genomic_resources/ciona_signaling.txt'
cycle_gene_file = '../data/genomic_resources/ciona_cycle.txt'

tfgroups = {'bHLH': 'bHLH',
            'bZIP': 'bZIP',
            'Ets': 'Ets',
            'Fox': 'fork head',
            'HMG': '(HMG)',
            'homeobox': 'Homeo',
            "Nuclear Receptor": "Nuclear hormone receptor, ligand-binding",
            "T-box": "T-box",
            "GATA": "GATA",
            "Generic tf": ["Transcription factor", "transcription factor"]}

signalinggroups = {'MAPK': "(MAP)",
                   'Fgf': 'Tyrosine-protein kinase, catalytic domain',
                   "GTPase": 'GTPase superfamily, Ras type',
                   'Beta receptor': 'beta receptor',
                   'Wnt': 'Wnt',
                   "hedgehog": "Hedgehog",
                   "JAK/STAT": ["STAT", "SOCS"],
                   "NFkB": 'NF-kappa-B',
                   "Notch": ["Notch", "Fringe"]}

zincfinger = {"Zinc finger": "Zinc finger"}

othergroups = {"Histone": "Histone",
               'Ribosome': 'Ribosome',
               "RNAP": "RNA polymerase",
               "Mitochondrial": "Mitochondrial",
               "ATPase": "ATPase",
               "RNase": "Ribonuclease",
               "RI": "ribonuclease inhibitor",
               "Ribosomal protein": "Ribosomal protein",
               "Histone-fold": "Histone-fold",
               "TGF-b": "Transforming growth factor-beta",
               "Integrin": "integrin",
               "Golgi": "Golgi"}


def get_ciona_genenames(genenamefile=genenamefile, columns=["Gene ID", "Gene Name"]):
    genenames = _pd.read_csv(genenamefile, sep='\t')
    genenames.columns = columns

    return genenames


def fix_gene_annotation(df, group, field='Definition'):

    for k, v in group.items():

        if isinstance(v, list):
            vec = _np.zeros(df.shape[0]).astype(bool)
            for val in v:
                tmp = _np.array([val in x for x in df[field]])
                vec = vec | tmp
        else:

            vec = _np.array([v in x for x in df[field]])

        df[k] = vec


def load_gene_annotations(annotationf=geneannotationf, genenamefile=genenamefile, clean=True, tfgroups=tfgroups, signalinggroups=signalinggroups, zincfinger=zincfinger, othergroups=othergroups, version="KH2013"):

    ann = _pd.read_csv(annotationf, sep='\t')

    del ann['Evidence'], ann['Ipro']
    ann = ann.drop_duplicates()

    ann['Gene ID'] = [version + ':' + g for g in ann['Gene']]
    ann['Type'] = [n.split("_")[0].strip() for n in ann['Name']]

    fix_gene_annotation(ann, tfgroups)

    ann['all TFs'] = ann[list(tfgroups.keys())].sum(1).astype(bool)

    fix_gene_annotation(ann, signalinggroups)

    ann['all signaling'] = ann[list(signalinggroups.keys())].sum(1).astype(bool)

    fix_gene_annotation(ann, zincfinger)

    fix_gene_annotation(ann, othergroups)

    ann = ann.rename(columns={"Gene name": "non unique ID"})

    genenames = get_ciona_genenames(genenamefile=genenamefile)

    ann = genenames.merge(ann, on="Gene ID", how='left')

    if clean:
        remfield = ['Gene', 'non unique ID', 'Name', 'Definition', 'Type']
        for f in remfield:
            del ann[f]

        ann = ann.drop_duplicates()
        ann = ann.fillna(False)
        ann = ann.groupby(["Gene ID", "Gene Name"], as_index=False).sum()

    return ann


def add_ghost_annotations(data, ghosttf="../data/genomic_resources/TFs_from_GHOST.tsv",
                          ghostsignal="../data/genomic_resources/signaling_molecules_from_GHOST.tsv"):

    tfs = _pd.read_csv(ghosttf, sep='\t')
    signl = _pd.read_csv(ghostsignal, sep="\t")

    data.var['GHOST TF'] = [x in tfs['GeneID'].values for x in data.var['Gene ID']]
    data.var['GHOST Signal'] = [x in signl['GeneID'].values for x in data.var['Gene ID']]
    data.var['GHOST TF'] = data.var['GHOST TF'].astype(int)
    data.var['GHOST Signal'] = data.var['GHOST Signal'].astype(int)


def add_annotation_to_data_var(data, annotations, merge_key='Gene ID', genenames="Gene Name"):

    annotations.index = annotations.index.astype(str)  # anndata does not like integers
    # adata.var_names = adata.var_names.astype(int)
    __ = data.var.merge(annotations, on=merge_key, how="left")
    # __.fillna(0.0)  # Why this line?
    __.index = __.index.astype(str)  # anndata does not like integers
    # __.index = adata.var.index
    if __[genenames].isna().any():
        __.loc[__[genenames].isna(), genenames] = __[merge_key][__[genenames].isna()].values.copy()

    # data = data[__[merge_key].values, :]
    # print(data.shape)

    __ = __.drop_duplicates()
    __ = __[__[merge_key].isin(data.var_names).values]

    data.var = __.copy()
    # data.var[genenames] = __[genenames].copy()
    data.var.set_index(genenames, inplace=True)


def load_signaling_genes(f=signaling_gene_file):

    signaling_genes = _pd.read_csv(f, sep='\t')

    signaling_genes = signaling_genes[['Ciona_name', 'Pathway']]

    return signaling_genes


def load_cell_cycle_genes(f=cycle_gene_file):

    cc = _pd.read_csv(f, sep='\t')

    # cc = cc[['Ciona_name', 'Pathway']]

    return cc
