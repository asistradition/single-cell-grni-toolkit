import os as _os
import pandas as _pd
import scanpy as _sc
from glob import glob as _glob
import pprint as _pp

datapath = _os.path.join('..', 'data', 'BEELINE_data')
configfiles = _os.path.join(datapath, 'config-files')


def load_dataset(confp=configfiles, datapath=datapath, datarep=None, dclass='Synthetic', dataset='dyn-LI.yaml', return_file=False):
    from yaml import load as _yload

    try:
        from yaml import CLoader as _yLoader
    except ImportError:
        from yaml import Loader as _yLoader

    isnone = False
    if dataset is None:
        dataset = '*'
        isnone = True
    if dclass is None:
        dclass = '*'
        dataset = '*'
        isnone = True

    if isnone:
        dirs = _glob(_os.path.join(confp, dclass, dataset))
        _pp.pprint(dirs)
        return None, None

    dpath = _os.path.join(confp, dclass, dataset)
    with open(dpath) as file:
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format
        data = _yload(file, Loader=_yLoader)

        if return_file:
            return data, None

    if datarep is None:
        print(_pd.DataFrame(data['input_settings']['datasets'])['name'].to_string())
        return None, None

    if isinstance(datarep, str):
        name = datarep
        df = _pd.DataFrame(data['input_settings']['datasets'])['name']
        datarep = df[df == name].index[0]

    else:
        name = _pd.DataFrame(data['input_settings']['datasets'])['name'][datarep]

    # if isinstance(datarep, str) else data['input_settings']['datasets'][datarep]['name']

    datasetpath = _os.path.join(datapath, data['input_settings']['input_dir'], data['input_settings']['dataset_dir'], name)

    adata = _sc.read_csv(_os.path.join(datasetpath, data['input_settings']['datasets'][datarep]['exprData'])).T

    adata.uns['name'] = name

    cellData = _pd.read_csv(_os.path.join(datasetpath, data['input_settings']['datasets'][datarep]['cellData']), index_col=0)

    adata.obs = adata.obs.merge(cellData, left_index=True, right_index=True)

    network = _pd.read_csv(_os.path.join(datasetpath, data['input_settings']['datasets'][datarep]['trueEdges']))

    return adata, network
